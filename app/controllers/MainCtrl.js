define([
  'angular',
  'app'
], function (angular, myApp) {



    myApp.factory('Data', function () {
        return {
            message: "I'm data from a service"
        };
    });

    myApp.filter('reverse', function (Data) {
        return function (text) {
            return text.split("").reverse().join("");
        };
    });

    // define our controller and register it with our app
    myApp.controller("MainCtrl", function ($scope, Data) {
        $scope.data = Data;

        $scope.zoomIn = function(){
            console.log("worked");
            $scope.data.map.setZoom(15);
        };

    });

});
