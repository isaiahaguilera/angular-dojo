define([
    "angular",
    "app",
    "esri/map",
], function (angular, myApp, Map) {

    // define our controller and register it with our app
    myApp.controller("MapCtrl", function ($scope, Data) {

        var map = new Map("map", {
            basemap: "topo",
            center: [-122.45, 37.75],
            zoom: 13,
            sliderStyle: "small"
        });
        Data.map = map;
    });

});
