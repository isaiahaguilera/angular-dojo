define([
  'angular',
  'app'
], function (angular, myApp) {

    // define our controller and register it with our app
    myApp.controller("SecondCtrl", function ($scope, Data) {
        $scope.title = "Hello World";
        $scope.data = Data;
    });
});
